import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';

bool isNAN(String string) {
  final numericRegex = RegExp(r'^-?(([0-9]*)|(([0-9]*)\.([0-9]*)))$');

  return !numericRegex.hasMatch(string);
}

class AddTransaction extends StatefulWidget {
  final List<Transaction> transactions;
  final Function addNewTransaction;

  AddTransaction(this.transactions, this.addNewTransaction);

  @override
  _AddTransactionState createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime _pickedDate;

  void _add() {
    final Transaction transaction = Transaction(
      id: DateTime.now().toString(),
      title: _titleController.text,
      amount: double.parse(_amountController.text),
      date: _pickedDate,
    );

    widget.addNewTransaction(widget.transactions, transaction);

    print(_titleController.text);
    print(_amountController.text);
  }

  void _onSubmit() {
    if (_titleController.text.isEmpty ||
        _amountController.text.isEmpty ||
        isNAN(_amountController.text) ||
        _pickedDate == null) {
      return;
    }

    final amnt = double.parse(_amountController.text);

    if (amnt <= 0) {
      return;
    }

    _add();

    Navigator.of(context).pop();
  }

  void _displayDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime.now(),
    ).then((date) {
      if (date == null) return;
      setState(() {
        _pickedDate = date;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextField(
              decoration: InputDecoration(labelText: 'Title'),
              controller: _titleController,
              // onChanged: (value) => this.textTitle = value,
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Amount'),
              controller: _amountController,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              onSubmitted: (_) => _onSubmit(),
              // onChanged: (value) => this.textAmount = value,
            ),
            SizedBox(
              height: 70,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(_pickedDate == null
                      ? 'Please pick a date!'
                      : 'Date picked: ${DateFormat.yMMMd().format(_pickedDate)}'),
                  FlatButton(
                    child: Text(
                      'Date',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                    textColor: Theme.of(context).primaryColor,
                    onPressed: _displayDatePicker,
                  )
                ],
              ),
            ),
            RaisedButton(
              child: Text('Add Transaction'),
              color: Theme.of(context).primaryColor,
              textColor: Theme.of(context).textTheme.button.color,
              onPressed: _onSubmit,
            ),
          ],
        ),
      ),
    );
  }
}
