import 'package:flutter/material.dart';
import './add_transaction.dart';
import './transaction_list.dart';
import '../models/transaction.dart';

class UserTransactions extends StatefulWidget {
  final Function deleteTransaction;
  UserTransactions(this.deleteTransaction);

  @override
  _UserTransactionsState createState() => _UserTransactionsState();
}

class _UserTransactionsState extends State<UserTransactions> {
  final List<Transaction> _userTransactions = [
    Transaction(
      id: 't1',
      title: 'Oraimo Watch',
      amount: 50,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't2',
      title: 'Apple Watch',
      amount: 300,
      date: DateTime.now(),
    ),
  ];

  void _addNewTransaction(
      List<Transaction> transactions, Transaction transaction) {
    setState(() {
      transactions.add(transaction);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AddTransaction(_userTransactions, _addNewTransaction),
        TransactionList(_userTransactions, widget.deleteTransaction),
      ],
    );
  }
}
